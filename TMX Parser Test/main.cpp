#include <iostream>
#include <vector>
#include <sstream>
#include "pugixml.hpp"

using namespace std;

int main(int argc, char **argv)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file("test.tmx");
	for (pugi::xml_node node = doc.child("map").first_child(); node; node = node.next_sibling())
	{
		std::string name = node.name();
		if (name == "tileset")
			continue;
		if (name == "layer")
		{
			for (pugi::xml_node chunk = node.child("data").first_child(); chunk; chunk = chunk.next_sibling())
			{
				int x, y, w, h, id;
				vector<int> ids;
				string data = chunk.text().as_string();
				stringstream ss(data);
				x = chunk.attribute("x").as_int();
				y = chunk.attribute("y").as_int();
				w = chunk.attribute("width").as_int();
				h = chunk.attribute("height").as_int();
				cout << "chunk{" << x << "," << y << "," << w << "," << h << "}\nRaw data:"<<data<<endl;
				for (int i = 0; i < w; i++)
				{
					for (int j = 0; j < h; j++)
					{
						if (ss >> id)
						{
							ids.push_back(id);
							if (ss.peek() == ',' || ss.peek() == '\r' || ss.peek() == '\n' || ss.peek() == ' ')
								ss.ignore();
						}
					}
				}

				cout << "Received data:\n";
				for (int i = 0; i < w; i++)
				{
					for (int j = 0; j < h; j++)
					{
						cout << ids[i*w + j] << " ";
					}
					cout << "\n";
				}
			}
		}
	}
	//cout << "error: " << image.attribute("error").value()<<endl;
	return 0;
}