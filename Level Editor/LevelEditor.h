#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_LevelEditor.h"

enum class Tool{PlaceRemove,Select};

class LevelEditor : public QMainWindow
{
	Q_OBJECT

public:
	LevelEditor(QWidget *parent = Q_NULLPTR);

public slots:
	void SelectTool(Tool t);
private:
	Ui::LevelEditorClass ui;
	Tool current_tool;
};
