#pragma once
#include <string>
#include <qgraphicsscene.h>
#include <qgraphicsitem.h>
class Object
{
protected:
	QGraphicsPixmapItem *pixmap;
public:
	virtual std::string GetType() = 0;
	QGraphicsPixmapItem *GetItem() { return pixmap; }
	Object();
	~Object();
};