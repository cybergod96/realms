#pragma once
#include <qvector.h>
#include <qrect.h>
#include <qpixmap.h>
#include <qvector2d.h>
class Tileset
{
private:
	QVector<QRect> tiles;
	QPixmap pixmap;
	QVector2D size;
public:
	Tileset();
	void Load(const QString &name);
	QPixmap *GetTilePixmap(int x, int y);
	QPixmap *GetTilePixmap(int id);
	int GetTilesCount() { return tiles.size(); }
};