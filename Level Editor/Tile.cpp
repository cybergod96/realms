#include "Tile.h"

Tile::Tile(Tileset *tileset, int tile_id, QVector2D position)
{
	this->tileset = tileset;
	SetTileId(tile_id);
	pixmap->setPos(position.x(), position.y());
}

Tile::~Tile()
{

}

void Tile::SetTileset(Tileset *t)
{
	tileset = t;
	if (tile_id > t->GetTilesCount())
		SetTileId(0);
}

void Tile::SetTileId(int id)
{
	if (!tileset) return;
	tile_id = id;
	pixmap->setPixmap(*tileset->GetTilePixmap(id));
}