#pragma once
#include "Tile.h"

class Platform : public Tile
{
private:
	QGraphicsRectItem *solidSelection;
public:
	Platform(Tileset *tileset, int tile_id, QVector2D position);
};