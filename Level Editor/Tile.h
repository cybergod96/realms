#pragma once
#include <qgraphicsscene.h>
#include "Object.h"
#include "Tileset.h"
class Tile : public Object
{
private:
	Tileset *tileset;
	int tile_id;
public:
	std::string GetType() { return "Tile"; }
	void SetTileset(Tileset *t);
	void SetTileId(int id);
	Tile(Tileset *tileset, int tile_id, QVector2D position);
	~Tile();
};