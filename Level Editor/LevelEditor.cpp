#include "LevelEditor.h"
#include "qmessagebox.h"

LevelEditor::LevelEditor(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	//init ui
	SelectTool(Tool::PlaceRemove);

	//setup signal handlers
	connect(ui.cbTileset, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int index) {
		QMessageBox::information(this, "Information", QString("Current index: %1 ").arg(index));
	});

	connect(ui.actionTool_PlaceRemove, &QAction::triggered, [this]{
		SelectTool(Tool::PlaceRemove);
	});

	connect(ui.actionTool_Select, &QAction::triggered, [this]{
		SelectTool(Tool::Select);
	});
}

void LevelEditor::SelectTool(Tool t)
{
	switch (t)
	{
	case Tool::PlaceRemove: //Place/Remove
		ui.actionTool_PlaceRemove->setChecked(true);
		ui.actionTool_Select->setChecked(false);
		break;
	case Tool::Select: //Select
		ui.actionTool_PlaceRemove->setChecked(false);
		ui.actionTool_Select->setChecked(true);
		break;
	default:
		break;
	}
	current_tool = t;
}