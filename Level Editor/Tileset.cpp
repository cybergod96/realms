#include "Tileset.h"

void Tileset::Load(const QString &name)
{
	pixmap.load(name);
	size.setX(pixmap.size().width() / 32);
	size.setY(pixmap.size().height() / 32);

	for (int i = 0; i < size.x(); i++)
	{
		for (int j = 0; j < size.y(); j++)
		{
			tiles.push_back(QRect(i * 32, j * 32, 32, 32));
		}
	}
}

QPixmap *Tileset::GetTilePixmap(int x, int y)
{
	if (tiles.empty())
		return nullptr;
	return &pixmap.copy(tiles[x + y * size.x()]);
}

QPixmap *Tileset::GetTilePixmap(int id)
{
	if (tiles.empty())
		return nullptr;
	return &pixmap.copy(tiles[id]);
}
