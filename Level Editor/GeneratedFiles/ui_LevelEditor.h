/********************************************************************************
** Form generated from reading UI file 'LevelEditor.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LEVELEDITOR_H
#define UI_LEVELEDITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LevelEditorClass
{
public:
    QAction *actionFile_New;
    QAction *actionFile_Open;
    QAction *actionFile_Save;
    QAction *actionFie_SaveAs;
    QAction *actionTool_PlaceRemove;
    QAction *actionTool_Select;
    QAction *actionView_Layers_Background;
    QAction *actionView_Layers_Platforms;
    QAction *actionView_Layers_Foreground;
    QAction *actionView_Triggers;
    QAction *actionView_Objects;
    QAction *actionView_Enemies;
    QWidget *centralWidget;
    QToolBox *toolBox;
    QWidget *pageTiles;
    QLabel *lblTileset;
    QComboBox *cbTileset;
    QLabel *lblLayer;
    QComboBox *cbLayer;
    QGraphicsView *gvTileset;
    QWidget *pageObjects;
    QWidget *pageEnemies;
    QWidget *pageProperties;
    QGraphicsView *gvLevel;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuTool;
    QMenu *menuView;
    QMenu *menuLayers;

    void setupUi(QMainWindow *LevelEditorClass)
    {
        if (LevelEditorClass->objectName().isEmpty())
            LevelEditorClass->setObjectName(QStringLiteral("LevelEditorClass"));
        LevelEditorClass->resize(1060, 764);
        LevelEditorClass->setMinimumSize(QSize(1060, 764));
        LevelEditorClass->setMaximumSize(QSize(1060, 764));
        actionFile_New = new QAction(LevelEditorClass);
        actionFile_New->setObjectName(QStringLiteral("actionFile_New"));
        actionFile_Open = new QAction(LevelEditorClass);
        actionFile_Open->setObjectName(QStringLiteral("actionFile_Open"));
        actionFile_Save = new QAction(LevelEditorClass);
        actionFile_Save->setObjectName(QStringLiteral("actionFile_Save"));
        actionFie_SaveAs = new QAction(LevelEditorClass);
        actionFie_SaveAs->setObjectName(QStringLiteral("actionFie_SaveAs"));
        actionTool_PlaceRemove = new QAction(LevelEditorClass);
        actionTool_PlaceRemove->setObjectName(QStringLiteral("actionTool_PlaceRemove"));
        actionTool_PlaceRemove->setCheckable(true);
        actionTool_PlaceRemove->setChecked(true);
        actionTool_Select = new QAction(LevelEditorClass);
        actionTool_Select->setObjectName(QStringLiteral("actionTool_Select"));
        actionTool_Select->setCheckable(true);
        actionView_Layers_Background = new QAction(LevelEditorClass);
        actionView_Layers_Background->setObjectName(QStringLiteral("actionView_Layers_Background"));
        actionView_Layers_Background->setCheckable(true);
        actionView_Layers_Background->setChecked(true);
        actionView_Layers_Platforms = new QAction(LevelEditorClass);
        actionView_Layers_Platforms->setObjectName(QStringLiteral("actionView_Layers_Platforms"));
        actionView_Layers_Platforms->setCheckable(true);
        actionView_Layers_Platforms->setChecked(true);
        actionView_Layers_Foreground = new QAction(LevelEditorClass);
        actionView_Layers_Foreground->setObjectName(QStringLiteral("actionView_Layers_Foreground"));
        actionView_Layers_Foreground->setCheckable(true);
        actionView_Layers_Foreground->setChecked(true);
        actionView_Triggers = new QAction(LevelEditorClass);
        actionView_Triggers->setObjectName(QStringLiteral("actionView_Triggers"));
        actionView_Triggers->setCheckable(true);
        actionView_Triggers->setChecked(true);
        actionView_Objects = new QAction(LevelEditorClass);
        actionView_Objects->setObjectName(QStringLiteral("actionView_Objects"));
        actionView_Objects->setCheckable(true);
        actionView_Objects->setChecked(true);
        actionView_Enemies = new QAction(LevelEditorClass);
        actionView_Enemies->setObjectName(QStringLiteral("actionView_Enemies"));
        actionView_Enemies->setCheckable(true);
        actionView_Enemies->setChecked(true);
        centralWidget = new QWidget(LevelEditorClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        toolBox = new QToolBox(centralWidget);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        toolBox->setGeometry(QRect(800, 0, 260, 728));
        toolBox->setMinimumSize(QSize(260, 728));
        toolBox->setMaximumSize(QSize(260, 728));
        toolBox->setFrameShape(QFrame::NoFrame);
        pageTiles = new QWidget();
        pageTiles->setObjectName(QStringLiteral("pageTiles"));
        pageTiles->setGeometry(QRect(0, 0, 260, 632));
        lblTileset = new QLabel(pageTiles);
        lblTileset->setObjectName(QStringLiteral("lblTileset"));
        lblTileset->setGeometry(QRect(20, 40, 41, 16));
        cbTileset = new QComboBox(pageTiles);
        cbTileset->addItem(QString());
        cbTileset->addItem(QString());
        cbTileset->addItem(QString());
        cbTileset->setObjectName(QStringLiteral("cbTileset"));
        cbTileset->setGeometry(QRect(80, 40, 161, 22));
        lblLayer = new QLabel(pageTiles);
        lblLayer->setObjectName(QStringLiteral("lblLayer"));
        lblLayer->setGeometry(QRect(20, 12, 31, 16));
        cbLayer = new QComboBox(pageTiles);
        cbLayer->addItem(QString());
        cbLayer->addItem(QString());
        cbLayer->addItem(QString());
        cbLayer->setObjectName(QStringLiteral("cbLayer"));
        cbLayer->setGeometry(QRect(80, 10, 161, 22));
        gvTileset = new QGraphicsView(pageTiles);
        gvTileset->setObjectName(QStringLiteral("gvTileset"));
        gvTileset->setGeometry(QRect(2, 75, 256, 544));
        toolBox->addItem(pageTiles, QStringLiteral("Tiles"));
        pageObjects = new QWidget();
        pageObjects->setObjectName(QStringLiteral("pageObjects"));
        pageObjects->setGeometry(QRect(0, 0, 260, 632));
        toolBox->addItem(pageObjects, QStringLiteral("Objects"));
        pageEnemies = new QWidget();
        pageEnemies->setObjectName(QStringLiteral("pageEnemies"));
        pageEnemies->setGeometry(QRect(0, 0, 260, 632));
        toolBox->addItem(pageEnemies, QStringLiteral("Enemies"));
        pageProperties = new QWidget();
        pageProperties->setObjectName(QStringLiteral("pageProperties"));
        toolBox->addItem(pageProperties, QStringLiteral("Properties"));
        gvLevel = new QGraphicsView(centralWidget);
        gvLevel->setObjectName(QStringLiteral("gvLevel"));
        gvLevel->setGeometry(QRect(0, 0, 800, 736));
        LevelEditorClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(LevelEditorClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1060, 26));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuTool = new QMenu(menuBar);
        menuTool->setObjectName(QStringLiteral("menuTool"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        menuLayers = new QMenu(menuView);
        menuLayers->setObjectName(QStringLiteral("menuLayers"));
        LevelEditorClass->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuTool->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuFile->addAction(actionFile_New);
        menuFile->addAction(actionFile_Open);
        menuFile->addAction(actionFile_Save);
        menuFile->addAction(actionFie_SaveAs);
        menuTool->addAction(actionTool_PlaceRemove);
        menuTool->addAction(actionTool_Select);
        menuView->addAction(menuLayers->menuAction());
        menuView->addAction(actionView_Objects);
        menuView->addAction(actionView_Enemies);
        menuView->addAction(actionView_Triggers);
        menuLayers->addAction(actionView_Layers_Background);
        menuLayers->addAction(actionView_Layers_Platforms);
        menuLayers->addAction(actionView_Layers_Foreground);

        retranslateUi(LevelEditorClass);

        toolBox->setCurrentIndex(0);
        toolBox->layout()->setSpacing(0);
        cbLayer->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(LevelEditorClass);
    } // setupUi

    void retranslateUi(QMainWindow *LevelEditorClass)
    {
        LevelEditorClass->setWindowTitle(QApplication::translate("LevelEditorClass", "Realms - Level Editor", nullptr));
        actionFile_New->setText(QApplication::translate("LevelEditorClass", "New", nullptr));
        actionFile_Open->setText(QApplication::translate("LevelEditorClass", "Open", nullptr));
        actionFile_Save->setText(QApplication::translate("LevelEditorClass", "Save", nullptr));
        actionFie_SaveAs->setText(QApplication::translate("LevelEditorClass", "Save as...", nullptr));
        actionTool_PlaceRemove->setText(QApplication::translate("LevelEditorClass", "Place/remove", nullptr));
        actionTool_Select->setText(QApplication::translate("LevelEditorClass", "Select", nullptr));
        actionView_Layers_Background->setText(QApplication::translate("LevelEditorClass", "Background", nullptr));
        actionView_Layers_Platforms->setText(QApplication::translate("LevelEditorClass", "Platforms", nullptr));
        actionView_Layers_Foreground->setText(QApplication::translate("LevelEditorClass", "Foreground", nullptr));
        actionView_Triggers->setText(QApplication::translate("LevelEditorClass", "Triggers", nullptr));
        actionView_Objects->setText(QApplication::translate("LevelEditorClass", "Objects", nullptr));
        actionView_Enemies->setText(QApplication::translate("LevelEditorClass", "Enemies", nullptr));
        lblTileset->setText(QApplication::translate("LevelEditorClass", "Tileset", nullptr));
        cbTileset->setItemText(0, QApplication::translate("LevelEditorClass", "Tileset 1", nullptr));
        cbTileset->setItemText(1, QApplication::translate("LevelEditorClass", "Tileset 2", nullptr));
        cbTileset->setItemText(2, QApplication::translate("LevelEditorClass", "Tileset 3", nullptr));

        lblLayer->setText(QApplication::translate("LevelEditorClass", "Layer", nullptr));
        cbLayer->setItemText(0, QApplication::translate("LevelEditorClass", "Background", nullptr));
        cbLayer->setItemText(1, QApplication::translate("LevelEditorClass", "Platforms", nullptr));
        cbLayer->setItemText(2, QApplication::translate("LevelEditorClass", "Foreground", nullptr));

        toolBox->setItemText(toolBox->indexOf(pageTiles), QApplication::translate("LevelEditorClass", "Tiles", nullptr));
        toolBox->setItemText(toolBox->indexOf(pageObjects), QApplication::translate("LevelEditorClass", "Objects", nullptr));
        toolBox->setItemText(toolBox->indexOf(pageEnemies), QApplication::translate("LevelEditorClass", "Enemies", nullptr));
        toolBox->setItemText(toolBox->indexOf(pageProperties), QApplication::translate("LevelEditorClass", "Properties", nullptr));
        menuFile->setTitle(QApplication::translate("LevelEditorClass", "File", nullptr));
        menuTool->setTitle(QApplication::translate("LevelEditorClass", "Tool", nullptr));
        menuView->setTitle(QApplication::translate("LevelEditorClass", "View", nullptr));
        menuLayers->setTitle(QApplication::translate("LevelEditorClass", "Layers", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LevelEditorClass: public Ui_LevelEditorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LEVELEDITOR_H
