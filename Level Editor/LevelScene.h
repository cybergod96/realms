#pragma once
#include <qobject.h>
#include <qgraphicsscene.h>
class LevelScene: public QGraphicsScene
{
	Q_OBJECT
private:
	QSize size;
public:
	LevelScene();
	~LevelScene();
protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
};

