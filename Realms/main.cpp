#include "main.h"

int main(int argc, char **argv)
{
	g_logger.Log("[INFO] Working Directory: %s", GetLoadingPath(""));
	g_logger.Log("[INFO] Creating window (v-sync on)...");
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	window.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Realms", sf::Style::Titlebar | sf::Style::Close,settings);
	window.setVerticalSyncEnabled(true);
	
	g_logger.Log("[INFO] Setting FPS limit: %d...", FPS_LIMIT);
	window.setFramerateLimit(FPS_LIMIT);
	g_logger.Log("[INFO] Loading font...");

	if (!g_assetManager.LoadFont(GetLoadingPath("times.ttf")))
		return 1;

	g_logger.Log("[INFO] Loading assets...");
	if (!g_assetManager.LoadAssets(GetLoadingPath("assets")))
	{
		g_logger.Log("[ERROR] Failed to load required assets!");
		return 1;
	}
	if (!g_assetManager.LoadGfxSubdir(GetLoadingPath("assets"), "tilesets"))
	{
		g_logger.Log("[ERROR] Failed to load tilesets!");
		return 1;
	}

	//register states
	if (argc == 2)
	{
		g_logger.Log("Testing level: %s", argv[1]);
		g_state_machine.RegisterState(STATE_TESTLEVEL, new TestLevelState(argv[1]));
		g_state_machine.PushState(STATE_TESTLEVEL);
	}


	g_logger.Log("[INFO] Initialization done");
	g_logger.Log("[INFO] Working Directory: %s", GetLoadingPath(""));

	while (window.isOpen() && !g_state_machine.CanExit())
	{
		while (window.pollEvent(event))
		{
			g_state_machine.OnEvent(event);
			if (event.type == sf::Event::Closed)
				window.close();
		}
		g_state_machine.OnUpdate();

		window.clear();
		g_state_machine.OnDraw(window);
		window.display();
	}
	return 0;
}