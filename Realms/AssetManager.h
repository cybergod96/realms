#pragma once
#include <SFML/Graphics.hpp>
#include <map>
#include <filesystem>
#include "Singleton.h"
#include "Logger.h"
#include "utils.h"
typedef std::map<std::string, sf::Texture*> TextureMap;

class AssetManager : public Framework::Singleton<AssetManager>
{
private:
	TextureMap textures;
	sf::Font font;
public:
	AssetManager();
	~AssetManager();
	sf::Texture &GetTexture(std::string name);
	sf::Font &GetFont();
	bool LoadAssets(std::string path);
	bool LoadGfxSubdir(std::string path, std::string dir);
	bool LoadFont(std::string name);

	TextureMap GetGfxSubdirContent(std::string dir);
};

#define g_assetManager AssetManager::Instance()

