#pragma once
#include <SFML/Graphics.hpp>
#include <direct.h>
#include <fstream>
#include "utils.h"
#include "settings.h"
#include "StateMachine.h"
#include "Logger.h"
#include "AssetManager.h"
#include "state_id.h"

#include "TestLevelState.h"

sf::RenderWindow window;
sf::Event event;