#include "utils.h" 

std::string GetLoadingPath(std::string file_name)
{
	std::stringstream ss;
	ss << _getcwd(NULL, 0) << "\\" << file_name;
	return ss.str();
}

void ShowText(sf::RenderWindow &target, sf::Vector2f pos, sf::String str, sf::Font &font, unsigned size)
{
	sf::Text t(str, font, size);
	t.setPosition(pos.x, pos.y);
	target.draw(t);
}

void ShowText_Centered(sf::RenderWindow &target, sf::FloatRect center_box, sf::String str, sf::Font &font, unsigned size)
{
	sf::Text t(str, font, size);

	float x = center_box.left + (center_box.width - t.getLocalBounds().width) / 2;
	float y = center_box.top + (center_box.height - t.getLocalBounds().height) / 4;

	t.setPosition(x, y);

	target.draw(t);
}

float GetDistance(sf::Vector2f pos1, sf::Vector2f pos2)
{
	float vx = pos2.x - pos1.x;
	float vy = pos2.y - pos1.y;

	return sqrt((vx*vx) + (vy*vy));
}

sf::Vector2i TranslateToScreenPos(sf::View & view, sf::Vector2f point)
{
	sf::Vector2f viewCenter = view.getCenter();
	sf::Vector2f halfExtents = view.getSize() / 2.0f;
	sf::Vector2f translation = viewCenter - halfExtents;
	sf::Vector2f result = viewCenter - halfExtents;
	return sf::Vector2i((int)result.x, (int)result.y);
}

bool StartsWith(std::string prefix, std::string str)
{
	return str.rfind(prefix, 0) == 0;
}

bool HasExtension(std::string ext, std::string path)
{
	size_t found = path.find_first_of(".");
	if (found == std::string::npos) return false;
	std::string fext = path.substr(found);
	return ext == fext;
}