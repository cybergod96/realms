#include "TestLevelState.h"



void TestLevelState::UpdateAdjustments()
{
	/*sf::Vector2i zero = TranslateToScreenPos(camera, sf::Vector2f(0, 0));
	camAdjustmentUp.setPosition(sf::Vector2f(zero.x, zero.y + 3*SCREEN_HEIGHT / 8 + camAdjustmentUp.getSize().y));
	camAdjustmentDown.setPosition(sf::Vector2f(zero.x, zero.y + 6 * SCREEN_HEIGHT / 8 + camAdjustmentUp.getSize().y));

	camAdjustmentLeft.setPosition(sf::Vector2f(zero.x + 3*SCREEN_WIDTH / 8 + camAdjustmentLeft.getSize().x, zero.y));
	camAdjustmentRight.setPosition(sf::Vector2f(zero.x + 6 * SCREEN_WIDTH / 8 + camAdjustmentLeft.getSize().x, zero.y));*/
	/*sf::Vector2i zero = TranslateToScreenPos(camera, sf::Vector2f(0, 0));
	camAdjustmentUp.setPosition(sf::Vector2f(zero.x, zero.y + SCREEN_HEIGHT/2 - SCREEN_HEIGHT/8));
	camAdjustmentDown.setPosition(sf::Vector2f(zero.x, zero.y + SCREEN_HEIGHT/2 + SCREEN_HEIGHT/8));

	camAdjustmentLeft.setPosition(sf::Vector2f(zero.x + SCREEN_WIDTH/2 - SCREEN_WIDTH/8, zero.y));
	camAdjustmentRight.setPosition(sf::Vector2f(zero.x + SCREEN_WIDTH/2 + SCREEN_WIDTH/8, zero.y));*/

	sf::Vector2i zero = TranslateToScreenPos(camera, sf::Vector2f(0, 0));
	camAdjustmentUp.setPosition(sf::Vector2f(zero.x, zero.y + SCREEN_HEIGHT / 2 - level->GetPlayer()->GetSize().y - 4));
	camAdjustmentDown.setPosition(sf::Vector2f(zero.x, zero.y + SCREEN_HEIGHT / 2 + level->GetPlayer()->GetSize().y + 4));

	camAdjustmentLeft.setPosition(sf::Vector2f(zero.x + SCREEN_WIDTH / 2 - level->GetPlayer()->GetSize().x - 4, zero.y));
	camAdjustmentRight.setPosition(sf::Vector2f(zero.x + SCREEN_WIDTH / 2 + level->GetPlayer()->GetSize().x + 4, zero.y));
}

TestLevelState::TestLevelState(const char *path)
{
	level = new Level();
	camera = sf::View(sf::FloatRect(SCREEN_WIDTH/2, SCREEN_HEIGHT/2, SCREEN_WIDTH, SCREEN_HEIGHT));
	map_path = path;
	//moveCamera = false;
	showCameraAdjustments = false;
	cameraStateHorizontal = CameraState::Stop;
	cameraStateVertical = CameraState::Stop;
	adjustSpeedHorizontal = adjustSpeedVertical = 0.f;

	camAdjustmentUp.setSize(sf::Vector2f(SCREEN_WIDTH, 1));
	camAdjustmentDown.setSize(sf::Vector2f(SCREEN_WIDTH, 1));
	camAdjustmentLeft.setSize(sf::Vector2f(1, SCREEN_HEIGHT));
	camAdjustmentRight.setSize(sf::Vector2f(1, SCREEN_HEIGHT));

	/*camAdjustmentUp.setPosition(sf::Vector2f(0, SCREEN_HEIGHT / 4 + camAdjustmentUp.getSize().y));
	camAdjustmentDown.setPosition(sf::Vector2f(0, 3 * SCREEN_HEIGHT / 4 + camAdjustmentUp.getSize().y));

	camAdjustmentLeft.setPosition(sf::Vector2f(SCREEN_WIDTH / 4 + camAdjustmentLeft.getSize().x, 0));
	camAdjustmentRight.setPosition(sf::Vector2f(3 * SCREEN_WIDTH / 4 + camAdjustmentLeft.getSize().x, 0));*/
	//UpdateAdjustments();

	camAdjustmentUp.setFillColor(sf::Color::Yellow);
	camAdjustmentDown.setFillColor(sf::Color::Yellow);
	camAdjustmentLeft.setFillColor(sf::Color::Yellow);
	camAdjustmentRight.setFillColor(sf::Color::Yellow);

}


TestLevelState::~TestLevelState()
{
}

void TestLevelState::OnEnter(int previousStateId)
{
	//std::string path = GetLoadingPath("assets/levels/test.tmx");
	if (!level->Load(map_path))
	{
		g_logger.Log("Failed to load map %s", map_path.c_str());
		g_state_machine.PopAllStates();
		return;
	}
	camera.setCenter(level->GetPlayer()->GetPositionPixels());
	camera.setCenter(floor(level->GetPlayer()->GetPositionPixels().x), floor(level->GetPlayer()->GetPositionPixels().y));
	newCameraPos = camera.getCenter();
	g_logger.Log("Level loaded successfully.");
}

void TestLevelState::OnExit(int nextStateId)
{
	delete level;
}

void TestLevelState::OnSuspend(int pushedStateId)
{
}

void TestLevelState::OnResume(int poppedStateId)
{
}

void TestLevelState::OnDraw(sf::RenderWindow & target, bool suspended)
{
	target.setView(camera);
	level->OnDraw(target);
	if(showCameraAdjustments)
	{
		target.draw(camAdjustmentLeft);
		target.draw(camAdjustmentRight);
		target.draw(camAdjustmentUp);
		target.draw(camAdjustmentDown);
	}
	target.setView(target.getDefaultView());
}

void TestLevelState::OnEvent(sf::Event & event, bool suspended)
{
	level->OnEvent(event);
	switch (event.type)
	{
	case sf::Event::KeyPressed:
		switch (event.key.code)
		{
		case sf::Keyboard::Space:
			level->GetPlayer()->SetPositionPixels(level->GetStartPoint());
			camera.setCenter(level->GetPlayer()->GetPosition());
			newCameraPos = camera.getCenter();
			UpdateAdjustments();
			break;
		case sf::Keyboard::P:
			{
				sf::FloatRect sr = level->GetPlayer()->GetScreenRect(camera);
				sf::FloatRect r = level->GetPlayer()->GetRect();
				sf::Vector2f cc = camera.getCenter();
				
				sf::FloatRect cau = camAdjustmentUp.getGlobalBounds(),
					cad = camAdjustmentDown.getGlobalBounds(),
					cal = camAdjustmentLeft.getGlobalBounds(),
					car = camAdjustmentRight.getGlobalBounds();

				g_logger.Log("Player screen rect: {%f,%f,%f,%f}", sr.left, sr.top, sr.width, sr.height);
				g_logger.Log("Player rect: {%f,%f,%f,%f}", r.left, r.top, r.width, r.height);
				g_logger.Log("Camera center: {%f,%f}", cc.x, cc.y);

				g_logger.Log("camAdjustmentUp{%f,%f,%f,%f", cau.left, cau.top, cau.width, cau.height);
				g_logger.Log("camAdjustmentDown{%f,%f,%f,%f", cad.left, cad.top, cad.width, cad.height);
				g_logger.Log("camAdjustmentLeft{%f,%f,%f,%f", cal.left, cal.top, cal.width, cal.height);
				g_logger.Log("camAdjustmentRight{%f,%f,%f,%f", car.left, car.top, car.width, car.height);
			}
			break;
		case sf::Keyboard::O:
			showCameraAdjustments = !showCameraAdjustments;
			break;
		case sf::Keyboard::I:
			//moveCamera = true;
			newCameraPos.y -= ADJUST_STEP_VERTICAL;
			cameraStateVertical = CameraState::MoveBackward;
			adjustSpeedVertical = fmax(4.f, floor(fabs(level->GetPlayer()->GetVelocityVector().y)));
			break;
		case sf::Keyboard::K:
			//moveCamera = true;
			newCameraPos.y += ADJUST_STEP_VERTICAL;
			cameraStateVertical = CameraState::MoveForward;
			adjustSpeedVertical = fmax(4.f, floor(fabs(level->GetPlayer()->GetVelocityVector().y)));
			break;
		case sf::Keyboard::J:
			//moveCamera = true;
			newCameraPos.x -= ADJUST_STEP_HORIZONTAL;
			cameraStateHorizontal = CameraState::MoveBackward;
			adjustSpeedHorizontal = fmax(4.f, floor(fabs(level->GetPlayer()->GetVelocityVector().x)));
			break;
		case sf::Keyboard::L:
			//moveCamera = true;
			newCameraPos.x += ADJUST_STEP_HORIZONTAL;
			cameraStateHorizontal = CameraState::MoveForward;
			adjustSpeedHorizontal = fmax(4.f, floor(fabs(level->GetPlayer()->GetVelocityVector().x)));
			break;
		}
		break;
	default:
		break;
	}
}

void TestLevelState::OnUpdate(bool suspended)
{
	level->OnUpdate();


	//camera.setCenter(floor(level->GetPlayer()->GetPositionPixels().x),floor(level->GetPlayer()->GetPositionPixels().y));

	//process camera state
	switch (cameraStateHorizontal)
	{
	case CameraState::MoveForward:
		camera.move(/*ADJUST_SPEED*/adjustSpeedHorizontal, 0);
		if (camera.getCenter().x >= newCameraPos.x)
		{
			camera.setCenter(newCameraPos.x, camera.getCenter().y);
			cameraStateHorizontal = CameraState::Stop;
			adjustSpeedHorizontal = 0.f;
			//moveCamera = false;
		}
		break;
	case CameraState::MoveBackward:
		camera.move(/*-ADJUST_SPEED*/-adjustSpeedHorizontal, 0);
		if (camera.getCenter().x <= newCameraPos.x)
		{
			camera.setCenter(newCameraPos.x, camera.getCenter().y);
			cameraStateHorizontal = CameraState::Stop;
			adjustSpeedHorizontal = 0.f;
			//moveCamera = false;
		}
	case CameraState::Stop:
		break;
	}

	switch (cameraStateVertical)
	{
	case CameraState::MoveForward:
		camera.move(0, adjustSpeedVertical/*ADJUST_SPEED*/);
		if (camera.getCenter().y >= newCameraPos.y)
		{
			camera.setCenter(camera.getCenter().x, newCameraPos.y);
			cameraStateVertical = CameraState::Stop;
			adjustSpeedVertical = 0.f;
			//moveCamera = false;
		}
		break;
	case CameraState::MoveBackward:
		camera.move(0, -adjustSpeedVertical/*-ADJUST_SPEED*/);
		if (camera.getCenter().y <= newCameraPos.y)
		{
			camera.setCenter(camera.getCenter().x, newCameraPos.y);
			cameraStateVertical = CameraState::Stop;
			adjustSpeedVertical = 0.f;
			//moveCamera = false;
		}
	case CameraState::Stop:
		break;
	}

	/*sf::Vector2i zero = TranslateToScreenPos(camera, sf::Vector2f(0, 0));
	camAdjustmentUp.setPosition(sf::Vector2f(zero.x, zero.y + SCREEN_HEIGHT / 4 + camAdjustmentUp.getSize().y));
	camAdjustmentDown.setPosition(sf::Vector2f(zero.x, zero.y + 3 * SCREEN_HEIGHT / 4 + camAdjustmentUp.getSize().y));

	camAdjustmentLeft.setPosition(sf::Vector2f(zero.x + SCREEN_WIDTH / 4 + camAdjustmentLeft.getSize().x, zero.y));
	camAdjustmentRight.setPosition(sf::Vector2f(zero.x + 3 * SCREEN_WIDTH / 4 + camAdjustmentLeft.getSize().x, zero.y));*/

	UpdateAdjustments();

	//if (level->GetPlayer()->GetRect().intersects(camAdjustmentUp.getGlobalBounds())
	//	/*&& level->GetPlayer()->GetPosition().y > camAdjustmentUp.getGlobalBounds().top*/)
	//{
	//	cameraStateVertical = CameraState::MoveBackward;
	//	newCameraPos.y = floor(level->GetPlayer()->GetPosition().y);
	//	adjustSpeedVertical = fmax(ADJUST_SPEED, floor(fabs(level->GetPlayer()->GetBody()->GetLinearVelocity().y)));
	//}

	//if (level->GetPlayer()->GetRect().intersects(camAdjustmentDown.getGlobalBounds())
	//	/*&& level->GetPlayer()->GetPosition().y < camAdjustmentUp.getGlobalBounds().top*/)
	//{
	//	cameraStateVertical = CameraState::MoveForward;
	//	newCameraPos.y = floor(level->GetPlayer()->GetPosition().y);
	//	adjustSpeedVertical = fmax(ADJUST_SPEED, floor(fabs(level->GetPlayer()->GetBody()->GetLinearVelocity().y)));
	//}

	//if (level->GetPlayer()->GetRect().intersects(camAdjustmentLeft.getGlobalBounds())
	//	/*&& level->GetPlayer()->GetPosition().x > camAdjustmentUp.getGlobalBounds().left*/)
	//{
	//	cameraStateHorizontal = CameraState::MoveBackward;
	//	newCameraPos.x = floor(level->GetPlayer()->GetPosition().x);
	//	adjustSpeedHorizontal = fmax(ADJUST_SPEED, floor(fabs(level->GetPlayer()->GetBody()->GetLinearVelocity().x)));
	//}

	//if (level->GetPlayer()->GetRect().intersects(camAdjustmentRight.getGlobalBounds())
	//	/*&& level->GetPlayer()->GetPosition().x < camAdjustmentUp.getGlobalBounds().left*/)
	//{
	//	cameraStateHorizontal = CameraState::MoveForward;
	//	newCameraPos.x = floor(level->GetPlayer()->GetPosition().x);
	//	adjustSpeedHorizontal = fmax(ADJUST_SPEED , floor(fabs(level->GetPlayer()->GetBody()->GetLinearVelocity().x)));
	//}

	//camera.setCenter(roundf(level->GetPlayer()->GetPositionPixels().x), roundf(level->GetPlayer()->GetPosition().y));

	camera.setCenter(roundf(level->GetPlayer()->GetPositionPixels().x),roundf(level->GetPlayer()->GetPositionPixels().y));

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) camera.move(0, -CAMERA_SPEED);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) camera.move(0, CAMERA_SPEED);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) camera.move(-CAMERA_SPEED, 0);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) camera.move(CAMERA_SPEED, 0);
}
