#pragma once
#include <SFML/Graphics.hpp>
#include <direct.h>
#include <string>
#include <sstream>
#include <cmath>
#include <Box2D/Box2D.h>

#define PX2M(px) (px)*0.02f
#define M2PX(m) (m)*50
#define DEG2RAD(d) (d)*b2_pi/180
#define RAD2DEG(r) 180*(r)/b2_pi

std::string GetLoadingPath(std::string file_name);
void ShowText(sf::RenderWindow &target, sf::Vector2f pos, sf::String str, sf::Font &font, unsigned size);
void ShowText_Centered(sf::RenderWindow &target, sf::FloatRect center_box, sf::String str, sf::Font &font, unsigned size);
float GetDistance(sf::Vector2f pos1, sf::Vector2f pos2);
sf::Vector2i TranslateToScreenPos(sf::View &view, sf::Vector2f point);
bool StartsWith(std::string prefix, std::string str);
bool HasExtension(std::string ext, std::string path);