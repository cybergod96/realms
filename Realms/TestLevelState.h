#pragma once
#include "State.h"
#include "Level.h"
#include "utils.h"
#include "StateMachine.h"
#include "settings.h"

enum class CameraState { MoveForward, MoveBackward, Stop };

//TODO: doda� przesuwanie kamery po kolizji z odpowiednio ustawionnymi RectangleShape

class TestLevelState : public State
{
private:
	Level *level;
	sf::View camera;
	std::string map_path;
	bool showCameraAdjustments;
	sf::Vector2f newCameraPos;
	CameraState cameraStateHorizontal, cameraStateVertical;
	float adjustSpeedHorizontal, adjustSpeedVertical;
	sf::RectangleShape camAdjustmentUp, camAdjustmentDown, camAdjustmentLeft, camAdjustmentRight;

	void UpdateAdjustments();
public:
	TestLevelState(const char *path);
	~TestLevelState();
	void OnEnter(int previousStateId);
	void OnExit(int nextStateId);
	void OnSuspend(int pushedStateId);
	void OnResume(int poppedStateId);
	void OnDraw(sf::RenderWindow &target, bool suspended);
	void OnEvent(sf::Event &event, bool suspended);
	void OnUpdate(bool suspended);
};

