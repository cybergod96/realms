#pragma once
#include <SFML/Graphics.hpp>
#include <vector>

/*
All frames have the same size

Animation file format (example file):
$file = "enemy.png"
[idle]
$framesNumber = 6;
$frameTime = 200;
$frameWidth = 24;
$frameHeight = 32;
$firstFrameX = 0;
$firstFrameY = 0;
$mode = Loop;

*/

enum class AnimationMode {PlayOnce, RepeatNTimes, Loop};

class Animation
{
private:
	std::vector<sf::IntRect>frames;
	int frameTime; //milliseconds
	sf::Sprite sprite;
	//bool jumpToFirstFrameAfterFinish; //true: 1,2,3,1,2,3...; false: 1,2,3,2,1,2,3,2,1...
	bool started, stopped;
	int remainingRepetitions, currentFrame;
	AnimationMode mode;
	sf::Clock frameClock;
public:
	Animation() {}
	~Animation() {}

	sf::Sprite &GetSprite() { return sprite; };

	//void SetJumpToFirstFrameAfterFinish(bool b) { jumpToFirstFrameAfterFinish = b; }
	void Play(float frameTimeMultiplier = 1.0f);
	void Stop() { started = false; stopped = true; }
	void Init(AnimationMode mode, sf::Texture &texture, int repetitions = 1);
	bool HasFinished();
	void Reset() { currentFrame = 0; started = stopped = false; }
};

