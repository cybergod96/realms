#include "Tileset.h"



Tileset::Tileset()
{
}


Tileset::~Tileset()
{
}

void Tileset::Load(std::string name)
{
	//g_logger.Log("***** Tileset::Load() *****");
	this->name = name;
	tiles.clear();
	tiles.push_back(sf::IntRect());
	//int counter = 1;
	texture = g_assetManager.GetTexture("tilesets/" + name);
	tileset_size.x = texture.getSize().x / 32;
	tileset_size.y = texture.getSize().y / 32;

	for (int i = 0; i < tileset_size.y; i++)
	{
		for (int j = 0; j < tileset_size.x; j++)
		{
			sf::IntRect r = sf::IntRect(j * 32, i * 32, 32, 32);
			//g_logger.Log("counter = %d", counter);
			//g_logger.Log("IntRect{%d,%d,%d,%d}", r.left, r.top, r.width, r.height);
			tiles.push_back(r);
			//counter++;
		}
	}
	//g_logger.Log("**********");
}

sf::IntRect *Tileset::GetTileRect(int x, int y)
{
	//g_logger.Log("***** Tileset::GetTileRect(x = %d,y = %d) *****",x,y);
	if (tiles.empty())
	{
		sf::IntRect empty(0, 0, 0, 0);
		return &empty;
	}
	//sf::IntRect r = tiles[x + y * tileset_size.x];
	//g_logger.Log("IntRect{%d,%d,%d,%d", r.left, r.top, r.width, r.height);
	//g_logger.Log("tile_id = %d", x + y * tileset_size.x);
	//g_logger.Log("**********");
	return &tiles[x + y * tileset_size.x];
}

sf::IntRect *Tileset::GetTileRect(int tile_id)
{
	//g_logger.Log("Tileset::GetTileRect(tile_id = %d) *****", tile_id);
	if (tiles.empty())
	{
		sf::IntRect empty(0,0,0,0);
		return &empty;
	}
	//sf::IntRect r = tiles[tile_id];
	//g_logger.Log("IntRect{%d,%d,%d,%d}", r.left, r.top, r.width, r.height);
	return &tiles[tile_id];
}
