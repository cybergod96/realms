#include "Platform.h"

Platform::Platform(b2World *world, sf::FloatRect rect, bool originBasedPosition, Tileset *tileset, int tile_id)
{
	InitAsPolygonEdgeHybridBody(world, rect, b2_staticBody, nullptr, originBasedPosition);
	SetTileset(tileset);
	SetTileId(tile_id);
	SetSpritePosition(body->GetPosition().x*32.f, body->GetPosition().y*32.f);
	shape.setPosition(body->GetPosition().x*32.f, body->GetPosition().y*32.f);
}


Platform::~Platform()
{
}

void Platform::onInit() 
{ 
	body->SetUserData(this);
	shape.setSize(sf::Vector2f(32,32));
	shape.setFillColor(sf::Color::Blue);
	shape.setOrigin(size.x / 2, size.y / 2);
}

void Platform::onUpdate() 
{ 
	//
}

void Platform::onDraw(sf::RenderWindow &target)
{	
	//target.draw(shape);
	DrawTile(target);
}

void Platform::onEvent(const sf::Event &e) { return; }

void Platform::onCollisionBegin(PhysicsObject *other) 
{
	return;
}
void Platform::onCollisionEnd(PhysicsObject *other) 
{ 
	return;
}

void Platform::onDeinit() { return; }
void Platform::onDestroy() { return; }
