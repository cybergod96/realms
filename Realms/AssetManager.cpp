#include "AssetManager.h"

AssetManager::AssetManager()
{
}


AssetManager::~AssetManager()
{
	textures.clear();
}

sf::Texture &AssetManager::GetTexture(std::string name)
{
	if (textures.find(name) == textures.end())
	{
		g_logger.Log("[WARNING] Tried to get non-existent texture '%s'.", name.c_str());
		sf::Texture empty;
		return empty;
	}
	return *textures[name];
}

sf::Font & AssetManager::GetFont()
{
	return font;
}

bool AssetManager::LoadAssets(std::string path)
{
	//load textures
	sf::Texture tex;
	for (auto &p : std::experimental::filesystem::directory_iterator(path + "/gfx"))
	{		
		if (std::experimental::filesystem::is_directory(p))
			continue;
		textures[p.path().filename().generic_string()] = new sf::Texture();
		if (!textures[p.path().filename().generic_string()]->loadFromFile(p.path().generic_string()))
		{
			g_logger.Log("[ERROR] Could not load texture '%s'", p.path().filename().generic_string().c_str());
			return false;
		}
	}

	//load sounds
	//load misc
	g_logger.Log("[INFO] Loaded %d assets.", textures.size());
	std::map<std::string, sf::Texture*>::iterator it = textures.begin();
	std::map<std::string, sf::Texture*>::iterator end = textures.end();
	for (; it != end; ++it)
	{
		g_logger.Log("[DEBUG] Asset: %s", it->first.c_str());
	}
	return true;
}

bool AssetManager::LoadGfxSubdir(std::string path, std::string dir)
{
	for (auto &p : std::experimental::filesystem::directory_iterator(path + "/gfx/" + dir))
	{	
		if (std::experimental::filesystem::is_directory(p) || HasExtension(".tsx",p.path().generic_string()))
			continue;
		std::string key = dir + "/" + p.path().filename().generic_string();
		textures[key] = new sf::Texture();
		if (!textures[key]->loadFromFile(p.path().generic_string()))
		{
			g_logger.Log("[ERROR] Could not load texture '%s'", key.c_str());
			return false;
		}
	}
	return true;
}

TextureMap AssetManager::GetGfxSubdirContent(std::string dir)
{
	TextureMap result;
	for (auto const &i : textures)
	{
		if (StartsWith(dir, i.first))
		{
			result[i.first] = i.second;
		}
	}
	return result;
}

bool AssetManager::LoadFont(std::string name)
{
	if (!font.loadFromFile(name))
	{
		g_logger.Log("[ERROR] Could not load font '%s'!", name.c_str());
		return false;
	}
	return true;
}
