#pragma once
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include "Logger.h"
#include "PhysicsObject.h"

#define MAX_JUMPING_VELOCITY -7.f
#define JUMPING_STEP 0.5f

enum class WalkingState {MoveLeft, MoveRight, Stop};
enum class JumpingState {Jumping, Falling, Stop};

class Player : public PhysicsObject
{
private:
	sf::RectangleShape shape;
	WalkingState walking_state;
	JumpingState jumping_state;

public:
	Player(b2World *world, sf::FloatRect rect);
	Player() {}

	inline std::string GetType() { return "Player"; }

	void onInit();
	void onDeinit();
	void onUpdate();
	void onEvent(const sf::Event &e);
	void onDraw(sf::RenderWindow &target);
	void onCollisionBegin(PhysicsObject *other);
	void onCollisionEnd(PhysicsObject *other);
	void onDestroy();

	sf::Vector2f GetPosition() { return shape.getPosition(); }
	sf::FloatRect GetRect() { return shape.getGlobalBounds();/*sf::FloatRect(GetPosition(), shape.getSize());*/ }
};