#pragma once
#include "State.h"
#include "Logger.h"
#include "Singleton.h"
#include <vector>
#include <map>

class StateMachine : public Framework::Singleton<StateMachine>
{
private:
	std::vector<State *>states;
	std::map<int, State*>registered_states;
	sf::Clock deltaClock;
public:
	void RegisterState(int id, State *state); //ok
	bool IsRegistered(int id) const; //ok
	bool IsRegistered(State *state); //ok
	void PushState(int id); //ok
	void PopState(); //ok
	const int *StateId(State *state); //ok
	State *GetRegisteredState(int id); //ok
	void PopAllStates() { states.clear(); } //ok
	StateMachine();
	~StateMachine();
	void OnUpdate(); //ok
	void OnDraw(sf::RenderWindow &target); //ok
	void OnEvent(sf::Event &event); //ok
	bool CanExit();
	sf::Clock &GetDeltaClock() { return deltaClock; }
};

#define g_state_machine StateMachine::Instance()
