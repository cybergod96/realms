#include "Player.h"

Player::Player(b2World *world, sf::FloatRect rect)
{
	InitAsRectangularBody(world, rect, b2_dynamicBody);

	shape.setSize(sf::Vector2f(size.x, size.y));
	shape.setFillColor(sf::Color::Red);
	shape.setOrigin(size.x/2, size.y/2);

	walking_state = WalkingState::Stop;
	jumping_state = JumpingState::Stop;
}

void Player::onInit() 
{
	body->SetUserData(this);
	shape.setSize(sf::Vector2f(size.x, size.y));
	shape.setFillColor(sf::Color::Red);
	shape.setOrigin(size.x/2, size.y/2);

	walking_state = WalkingState::Stop;
	jumping_state = JumpingState::Stop;
}

void Player::onDeinit() { return; }
void Player::onDestroy() { return; }

void Player::onUpdate()
{
	b2Vec2 velocity = body->GetLinearVelocity();

	switch (walking_state)
	{
	case WalkingState::MoveLeft:
		velocity.x = b2Max(velocity.x - 0.3f, -5.0f);
		break;
	case WalkingState::MoveRight:
		velocity.x = b2Min(velocity.x + 0.3f, 5.0f);
		break;
	case WalkingState::Stop:
		if(jumping_state != JumpingState::Falling)velocity.x *= 0.97f;
		break;
	}

	switch (jumping_state)//TODO: change to apply impulse limited by velocity limit
	{
	case JumpingState::Jumping:
		if (velocity.y == 0) velocity.y = MAX_JUMPING_VELOCITY / 2;
		velocity.y = b2Max(velocity.y - JUMPING_STEP, MAX_JUMPING_VELOCITY);
		if (velocity.y <= MAX_JUMPING_VELOCITY) jumping_state = JumpingState::Falling;
		break;
	case JumpingState::Falling:
		if (velocity.y > 0)velocity.y *= 1.035f;
		//if (velocity.y == 0) jumping_state = JumpingState::Stop;
		break;
	case JumpingState::Stop:
		if (b2Abs(velocity.y) > 0) jumping_state = JumpingState::Falling;
		break;
	}
	body->SetLinearVelocity(velocity);
	shape.setPosition(roundf(body->GetPosition().x*32.f), roundf(body->GetPosition().y*32.f));
}

void Player::onEvent(const sf::Event &e)
{
	if (e.type == sf::Event::KeyPressed)
	{
		if (e.key.code == sf::Keyboard::Left) { walking_state = WalkingState::MoveLeft; }
		if (e.key.code == sf::Keyboard::Right) { walking_state = WalkingState::MoveRight; }
		if (e.key.code == sf::Keyboard::Up)
		{
			if (jumping_state == JumpingState::Stop)
				jumping_state = JumpingState::Jumping;
		}
	}
	if (e.type == sf::Event::KeyReleased)
	{
		if (e.key.code == sf::Keyboard::Left || e.key.code == sf::Keyboard::Right)
		{
			walking_state = WalkingState::Stop;
		}
		if (e.key.code == sf::Keyboard::Up)
		{
			if (jumping_state == JumpingState::Jumping)
				jumping_state = JumpingState::Stop;
		}
	}
}

void Player::onDraw(sf::RenderWindow &target)
{
	target.draw(shape);
}

void Player::onCollisionBegin(PhysicsObject *other) 
{ 
	if (other->GetType() == "Platform")
	{
		if(other->GetPositionPixels().y > GetPositionPixels().y && jumping_state == JumpingState::Falling)
			jumping_state = JumpingState::Stop;
		if (other->GetPositionPixels().y < GetPositionPixels().y && jumping_state == JumpingState::Jumping)
			jumping_state = JumpingState::Falling;
	}
}
void Player::onCollisionEnd(PhysicsObject *other) { return; }