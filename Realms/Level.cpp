#include "Level.h"



Level::Level()
{
	world = new b2World(b2Vec2(0, 9.81f));
	world->SetContactListener(&contact_listener);
}


Level::~Level()
{
	delete world;
}


void Level::OnUpdate()
{
	world->Step(1/60.f, 8, 3);
	for (int i = NUMLAYERS - 1; i >= 0; i--)
	{
		for (int j = 0; j < layers[i].size(); j++)
		{
			layers[i][j]->onUpdate();
		}
	}
}

void Level::OnEvent(sf::Event &event)
{
	for (int i = NUMLAYERS - 1; i >= 0; i--)
	{
		for (int j = 0; j < layers[i].size(); j++)
		{
			layers[i][j]->onEvent(event);
		}
	}
}

void Level::OnDraw(sf::RenderWindow &target)
{
	for (int i = 0; i < NUMLAYERS; i++)
	{
		for (int j = 0; j < layers[i].size(); j++)
		{
			layers[i][j]->onDraw(target);
		}
	}
}

void Level::Insert(Object *o, LAYER layer)
{
	if (layer < 0 || layer >= NUMLAYERS) return;
	o->onInit();
	layers[layer].push_back(o);
}

std::vector<Object *>Level::GetObjectsByType(std::string type, LAYER layer)
{
	std::vector<Object *> result;
	for (auto o : layers[layer])
	{
		result.push_back(o);
	}
	return result;
}

std::vector<Object *>Level::GetObjectsByType(std::string type)
{
	std::vector<Object *> result;
	for (int i = 0; i < NUMLAYERS; i++)
	{
		for (auto o : layers[i])
		{
			if (o->GetType() == type)
				result.push_back(o);
		}
	}
	return result;
}

bool Level::Load(std::string path)
{
	g_logger.Log("Loading level %s", path.c_str());
	//load map file
	pugi::xml_document docMap;
	pugi::xml_parse_result result = docMap.load_file(path.c_str());
	if (!result)
	{
		g_logger.Log("[ERROR] Map file parse error: %s", result.description());
		return false;
	}
	pugi::xml_node map_node = docMap.child("map");
	tile_size.x = map_node.attribute("tilewidth").as_int();
	tile_size.y = map_node.attribute("tileheight").as_int();
	for (pugi::xml_node node = map_node.first_child(); node; node = node.next_sibling())
	{
		std::string name = node.name();

		if (name == "tileset") //tileset info
		{
			std::string path_tileset = path + node.attribute("source").value();

			std::string fname;
			size_t found = path_tileset.find_last_of("/");
			if (found != std::string::npos)
				fname = path_tileset.substr(found + 1);
			fname = GetLoadingPath("assets/gfx/tilesets/" + fname);
			//g_logger.Log("Tileset path: %s", fname.c_str()/*path_tileset.c_str()*/);

			//load tileset
			g_logger.Log("Loading tileset %s", fname.c_str()/*path_tileset.c_str()*/);
			pugi::xml_document docTileset;
			result = docTileset.load_file(fname.c_str()/*path_tileset.c_str()*/);
			if (!result)
			{
				g_logger.Log("[ERROR] Tileset file parse error: %s", result.description());
				return false;
			}
			
			//get tileset image path
			std::string path_tileset_img = docTileset.child("tileset").child("image").attribute("source").value();
			//path_tileset_img = GetLoadingPath("assets/gfx/tilesets/" + path_tileset_img);
			//g_logger.Log("Tileset image path: %s", path_tileset_img.c_str());
			
			//load tileset image
			tileset.Load(path_tileset_img);
		}
		else if (name == "layer") //tile layer
		{
			std::string layer_name = node.attribute("name").value();
			if (layer_name == "Background")
			{
				ParseLayerNode(node, BACKGROUND);
			}
			else if (layer_name == "Platforms")
			{
				ParseLayerNode(node, PLATFORMS);
			}
			else if (layer_name == "Foreground")
			{
				ParseLayerNode(node, FOREGROUND);
			}
			else
			{
				g_logger.Log("[WARNING] Unsupported tile layer '%s'.", layer_name.c_str());
				continue;
			}
		}
		else if (name == "objectgroup") //object layer
		{
			std::string objectgroup_name = node.attribute("name").value();
			if (objectgroup_name == "Special") //triggers, waypoints, etc.
			{
				ParseObjectgroupNode(node, SPECIAL);
			}
			else if (objectgroup_name == "Objects") //traps, switches, collectibles, etc
			{
				ParseObjectgroupNode(node, OBJECTS);
			}
			else if (objectgroup_name == "Entities") //enemies, bosses, player
			{
				ParseObjectgroupNode(node, ENTITIES);
			}
			else
			{
				g_logger.Log("[WARNING] Unsupported object group '%s'.", objectgroup_name.c_str());
				continue;
			}
		}
		else
		{
			g_logger.Log("[WARNING] Unsupported tag '%s'.", name.c_str());
			continue;
		}
	}
	g_logger.Log("Inserting player.");
	player = new Player(world, sf::FloatRect(startPoint.x, startPoint.y, 24, 32));
	Insert(player, ENTITIES);
	/*g_logger.Log("Platforms #: %d", layers[PLATFORMS].size());
	for (auto p : layers[PLATFORMS])
	{
		Platform *x = dynamic_cast<Platform *>(p);
		g_logger.Log("Platform{%f,%f}", x->GetPositionPixels().x, x->GetPositionPixels().y);
	}*/
	g_logger.Log("Loading level finished.");
	return true;
}

void Level::ParseLayerNode(pugi::xml_node &node, LAYER layer)
{
	for (pugi::xml_node chunk_node = node.child("data").first_child(); chunk_node; chunk_node = chunk_node.next_sibling())
	{
		//g_logger.Log("Parsing chunk (node name = %s)",chunk_node.name());
		sf::IntRect chunk;
		chunk.left = chunk_node.attribute("x").as_int() * tile_size.x;
		chunk.top = chunk_node.attribute("y").as_int() * tile_size.y;
		chunk.width = chunk_node.attribute("width").as_int();
		chunk.height = chunk_node.attribute("height").as_int();
		std::string data = chunk_node.text().as_string();
		std::stringstream ss(data);
		int id;

		for (int i = 0; i < chunk.width; i++)
		{
			for (int j = 0; j < chunk.height; j++)
			{
				if (ss >> id)
				{
					//g_logger.Log("id = %d", id);
					if (id != 0)
					{
						sf::Vector2f pos;
						pos.x = chunk.left + j * tile_size.x;
						pos.y = chunk.top + i * tile_size.y;
						//g_logger.Log("pos{%f,%f}", pos.x, pos.y);
						//Object *obj;
						switch (layer)
						{
						case BACKGROUND: case FOREGROUND:
							//g_logger.Log("Background or Foreground layer.");
							/*obj = */Insert(new Tile(&tileset, id, pos), layer);
							break;
						case PLATFORMS:
							//g_logger.Log("Platforms layer.");
							/*obj = */Insert(new Platform(world, sf::FloatRect(pos, sf::Vector2f(32, 32)), true, &tileset, id), layer);
							break;
						default:
							break;
						}
						//Insert(obj, layer);
					}
					if (ss.peek() == ',' || ss.peek() == '\r' || ss.peek() == '\n' || ss.peek() == ' ')
						ss.ignore();					
				}
			}
		}
	}
}

void Level::ParseObjectgroupNode(pugi::xml_node &node, LAYER layer)
{
	for (pugi::xml_node object_node = node.first_child(); object_node; object_node = object_node.next_sibling())
	{
		std::string type = object_node.attribute("type").as_string();
		//g_logger.Log("Parsing objectgroup node");
		if (type == "StartPoint")
		{
			startPoint.x = object_node.attribute("x").as_int();
			startPoint.y = object_node.attribute("y").as_int();
			//g_logger.Log("StartPoint{%d,%d}", startPoint.x, startPoint.y);
		}
		else if (type == "EndPoint")
		{
			endPoint.x = object_node.attribute("x").as_int();
			endPoint.y = object_node.attribute("y").as_int();
			//g_logger.Log("EndPoint{%d,%d}", endPoint.x, endPoint.y);
		}
		else
		{
			g_logger.Log("[WARNING] Unsupported object type '%s'", type.c_str());
			continue;
		}
	}
}
