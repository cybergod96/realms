#include "TileRepresentation.h"



TileRepresentation::TileRepresentation()
{
	tile_id = 0;
}


TileRepresentation::~TileRepresentation()
{
}

void TileRepresentation::SetTileId(int id)
{
	tile_id = id;
	sprite.setTextureRect(*tileset->GetTileRect(id));
	sprite.setOrigin(sprite.getTextureRect().width / 2, sprite.getTextureRect().height / 2);
}

void TileRepresentation::SetTileId(int x, int y)
{
	if (x > tileset->GetSize().x || y > tileset->GetSize().y || x < 0 || y < 0)
		return;
	SetTileId(x + y * tileset->GetSize().x);
}

void TileRepresentation::SetTileset(Tileset *tileset)
{
	if (!tileset) return;
	this->tileset = tileset;
	sprite.setTexture(this->tileset->GetTexture());
	if(tile_id > tileset->GetTilesCount()-1) SetTileId(0);
}