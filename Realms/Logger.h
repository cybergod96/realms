#pragma once
#include <fstream>
#include <cstdarg>
#include "Singleton.h"
#include "utils.h"

class Logger : public Framework::Singleton<Logger>
{
private:
	std::fstream file;
public:
	Logger();
	~Logger();
	void Log(const char *format, ...);
};

#define g_logger Logger::Instance()

