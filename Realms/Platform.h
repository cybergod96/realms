#pragma once
#include "PhysicsObject.h"
#include "TileRepresentation.h"
#include "Logger.h"
class Platform : public PhysicsObject, public TileRepresentation
{
private:
	sf::RectangleShape shape;
public:
	Platform(b2World *world, sf::FloatRect rect, bool originBasedPosition/* = false*/, Tileset *tileset, int tile_id);
	Platform() {}
	~Platform();

	inline std::string GetType() { return "Platform"; }

	void onInit();
	void onDeinit();
	void onUpdate();
	void onEvent(const sf::Event &e);
	void onDraw(sf::RenderWindow &target);
	void onCollisionBegin(PhysicsObject *other);
	void onCollisionEnd(PhysicsObject *other);
	void onDestroy();

	sf::Vector2f GetShapePos() { return shape.getPosition(); }

	sf::Vector2f GetPosition() { return GetSpritePosition(); }
	sf::FloatRect GetRect() { return sf::FloatRect(GetSpritePosition(), sf::Vector2f(32, 32)); }
};

