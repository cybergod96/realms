#include "StateMachine.h"



void StateMachine::RegisterState(int id, State * state)
{
	if (!IsRegistered(id) && !IsRegistered(state))
		registered_states[id] = state;
}

bool StateMachine::IsRegistered(int id) const
{
	return registered_states.find(id) != registered_states.end();
}

bool StateMachine::IsRegistered(State * state)
{
	return StateId(state) != nullptr;
}

void StateMachine::PushState(int id)
{
	if (!IsRegistered(id)) return; //check if the state of given id is registered
	if (states.size() > 0) states.back()->OnSuspend(id); //inform state on the top of the stack that it is being suspended
	State *newState = GetRegisteredState(id); //get state pointer from registered states
	int previousStateId = -1;
	if(!states.empty()) previousStateId = *StateId(states.back()); //get current state id
	states.push_back(newState); //push new state onto the stack
	newState->OnEnter(previousStateId); //handle entering
}

void StateMachine::PopState()
{
	if (states.size() == 0) return; //leave if there is no states on the stack
	State *poppedState = states.back(); //get state from the top of the stack
	int poppedStateId = *StateId(poppedState); //get id of popped state
	int resumedStateId = (states.size() > 1) ? *StateId(states[states.size() - 2]) : -1; //get resumed state id (if there are more than one states on the stack)
	poppedState->OnExit(resumedStateId); //inform popped state about ongoing state
	states.pop_back(); //remove state from the top of the stack
	if (states.size() > 0) states.back()->OnResume(poppedStateId); //call OnResume current state on the top of the stack (if there is any)
}

const int * StateMachine::StateId(State * state)
{
	std::map<int, State*>::iterator it = registered_states.begin();
	std::map<int, State*>::iterator end = registered_states.end();
	for (; it != end; ++it)
	{
		if (it->second == state) return &it->first;
	}
	return nullptr;
}

State * StateMachine::GetRegisteredState(int id)
{
	return registered_states[id];
}

StateMachine::StateMachine()
{
}


StateMachine::~StateMachine()
{

}

void StateMachine::OnUpdate()
{
	if (states.empty()) return;
	//process active state
	states.back()->OnUpdate(false);
	//process suspended sates
	for (int i = states.size()-2; i >= 0; i--)
	{
		states[i]->OnUpdate(true);
	}
	deltaClock.restart();
}

void StateMachine::OnDraw(sf::RenderWindow & target)
{
	target.setView(target.getDefaultView());
	if (states.empty()) return;
	//process suspended sates
	for (int i = states.size() - 2; i >= 0; i--)
	{
		states[i]->OnDraw(target,true);
	}

	//process active state
	states.back()->OnDraw(target, false);
}

void StateMachine::OnEvent(sf::Event & event)
{
	if (states.empty()) return;
	//process active state
	states.back()->OnEvent(event,false);
	//process suspended sates
	for (int i = states.size() - 2; i >= 0; i--)
	{
		states[i]->OnEvent(event,true);
	}
}

bool StateMachine::CanExit()
{
	return states.empty();
}