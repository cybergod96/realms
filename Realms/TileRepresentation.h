#pragma once
#include "Tileset.h"
class TileRepresentation
{
private:
	Tileset *tileset;
	sf::Sprite sprite;
	int tile_id;
public:
	TileRepresentation();
	~TileRepresentation();
	void SetTileset(Tileset *tileset);
	void SetTileId(int id);
	void SetTileId(int x, int y);
	void DrawTile(sf::RenderWindow &target) { target.draw(sprite); }
	void SetSpritePosition(int x, int y) { sprite.setPosition(x, y); }
	void SetSpritePosition(sf::Vector2f position) { sprite.setPosition(position); }
	sf::Vector2f GetSpritePosition() { return sprite.getPosition(); }
};

