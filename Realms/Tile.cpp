#include "Tile.h"



Tile::Tile(Tileset *tileset, int tile_id, sf::Vector2f position)
{
	SetTileset(tileset);
	SetTileId(tile_id);
	SetSpritePosition(position);
}


Tile::~Tile()
{
}

void Tile::onInit() { return; }

void Tile::onDeinit() { return; }

void Tile::onUpdate() { return; }

void Tile::onEvent(const sf::Event & e) { return; }

void Tile::onDraw(sf::RenderWindow & target)
{
	DrawTile(target);
}

void Tile::onDestroy() { return; }
