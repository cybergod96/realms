#pragma once
#include <SFML/Graphics.hpp>

class State
{
public:
	State();
	virtual ~State();
	virtual void OnEnter(int previousStateId) = 0;
	virtual void OnExit(int nextStateId) = 0;
	virtual void OnSuspend(int pushedStateId) = 0;
	virtual void OnResume(int poppedStateId) = 0;
	virtual void OnDraw(sf::RenderWindow &target, bool suspended) = 0;
	virtual void OnEvent(sf::Event &event, bool suspended) = 0;
	virtual void OnUpdate(bool suspended) = 0;
};

