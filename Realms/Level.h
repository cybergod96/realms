#pragma once
#include <vector>
#include <sstream>
#include <SFML/Graphics.hpp>
#include "pugixml.hpp"
#include "Object.h"
#include "PhysicsObject.h"
#include "Tileset.h"
#include "Tile.h"
#include "Platform.h"
#include "Player.h"
#include "MyContactListener.h"
#include "StateMachine.h"

typedef std::vector<Object *>Layer;

enum LAYER { BACKGROUND, PLATFORMS, OBJECTS, ENTITIES, FOREGROUND, SPECIAL, NUMLAYERS };

class Level
{
private:
	b2World *world;
	Layer layers[NUMLAYERS];
	Tileset tileset;
	sf::Vector2i tile_size;
	void ParseLayerNode(pugi::xml_node &node, LAYER layer);
	void ParseObjectgroupNode(pugi::xml_node &node, LAYER layer);
	sf::Vector2i startPoint, endPoint;
	Player *player;
	MyContactListener contact_listener;
public:
	Level();
	~Level();

	b2World *GetWorld() { return world; }

	bool Load(std::string);
	
	void Insert(Object *o, LAYER layer);
	
	void OnDraw(sf::RenderWindow &target);
	void OnEvent(sf::Event &event);
	void OnUpdate();

	std::vector<Object *>GetObjectsByType(std::string type, LAYER layer);
	std::vector<Object *>GetObjectsByType(std::string type);

	Player *GetPlayer() { return player; }
	sf::Vector2i GetStartPoint() { return startPoint; }
	sf::Vector2i GetEndPoint() { return endPoint; }
};

