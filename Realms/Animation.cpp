#include "Animation.h"

void Animation::Init(AnimationMode mode, sf::Texture & texture, int repetitions)
{
	//jumpToFirstFrameAfterFinish = true;
	this->mode = mode;
	switch (mode)
	{
	case AnimationMode::PlayOnce:
		remainingRepetitions = 1;
		break;
	case AnimationMode::RepeatNTimes:
		remainingRepetitions = repetitions;
		break;
	case AnimationMode::Loop:
		remainingRepetitions = -1;
		break;
	}
	sprite.setTexture(texture);
	currentFrame = 0;
	sprite.setTextureRect(frames[currentFrame]);
	started = false;
}

void Animation::Play(float frameTimeMultiplier)
{
	if (!started && !stopped)
	{
		currentFrame = 0;
		frameClock.restart();
		started = true;
		stopped = false;
	}
	if (frameClock.getElapsedTime().asMilliseconds()/frameTimeMultiplier >= frameTime)
	{	
		switch (mode)
		{
		case AnimationMode::PlayOnce:
			if(currentFrame + 1 >= frames.size()) stopped = true;
			else
			{
				currentFrame++;
				//frameClock.restart();
			}
			break;
		case AnimationMode::RepeatNTimes:
			if (remainingRepetitions > 0)
			{
				if (currentFrame + 1 >= frames.size())
				{
					currentFrame = 0;
					remainingRepetitions--;
					//frameClock.restart();
				}
				else
				{
					currentFrame++;
					//frameClock.restart();
				}
			}
			break;
		case AnimationMode::Loop:
			currentFrame = (currentFrame+1)%frames.size();
			//frameClock.restart();
			break;
		}
		sprite.setTextureRect(frames[currentFrame]);
		frameClock.restart();
	}
}


bool Animation::HasFinished()
{
	if (mode == AnimationMode::Loop) return false;
	return stopped && currentFrame == frames.size() - 1 && remainingRepetitions == 0;
}
