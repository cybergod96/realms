#include "PhysicsObject.h"

//TODO: zamienić inicjalizację obiektów z konstruktora na InitAsRectangularBody()

PhysicsObject::PhysicsObject()
{
	id = "";
}


PhysicsObject::~PhysicsObject()
{
}

sf::Vector2f PhysicsObject::GetPositionPixels()
{
	return sf::Vector2f(body->GetPosition().x*32.f, body->GetPosition().y * 32.f);
}

sf::Vector2f PhysicsObject::GetPositionMeters()
{
	return sf::Vector2f(body->GetPosition().x, body->GetPosition().y);
}

void PhysicsObject::SetPositionPixels(int x, int y)
{
	body->SetTransform(b2Vec2((size.x / 2 + x)/32.f, (size.y / 2 + y)/32.f),body->GetAngle());
}

void PhysicsObject::SetPositionPixels(sf::Vector2i pos)
{
	body->SetTransform(b2Vec2((size.x / 2 + pos.x)/32.f, (size.y / 2 + pos.y)/32.f), body->GetAngle());
}

void PhysicsObject::InitAsEdgeBody(b2World *world, sf::FloatRect rect,
	b2BodyType type, void *userData, bool originBasedPosition, float friction,
	float restitution, bool isSensor)
{
	size = sf::Vector2f(rect.width, 1.f);

	b2BodyDef bodyDef;

	b2Vec2 bodyPosition;
	if (!originBasedPosition)
	{
		bodyPosition.x = (rect.left + size.x / 2) / 32.f; //pixels to meters
		bodyPosition.y = (rect.top + size.y / 2) / 32.f; //pixels to meters
	}
	else
	{
		bodyPosition.x = rect.left / 32.f; //pixels to meters
		bodyPosition.y = rect.top / 32.f; //pixels to meters
	}

	bodyDef.position = bodyPosition;
	bodyDef.type = type;
	body = world->CreateBody(&bodyDef);

	b2EdgeShape bodyShape;

	b2Vec2 v1 = b2Vec2(-0.5, 0),
		v2 = b2Vec2(0.5, 0),
		v0 = b2Vec2(-1, 0),
		v3 = b2Vec2(1.5, 0);

	bodyShape.Set(v1, v2);
	bodyShape.m_vertex0 = v0;
	bodyShape.m_vertex3 = v3;
	bodyShape.m_hasVertex0 = true; 
	bodyShape.m_hasVertex3 = true;

	b2FixtureDef fixtureDef;
	fixtureDef.friction = friction;
	fixtureDef.restitution = restitution;
	fixtureDef.isSensor = isSensor;
	fixtureDef.userData = userData;
	fixtureDef.shape = &bodyShape;
	body->CreateFixture(&fixtureDef);
}

void PhysicsObject::InitAsRectangularBody(b2World *world, sf::FloatRect rect, 
	b2BodyType type, void *userData, bool originBasedPosition, float friction, 
	float restitution, bool isSensor)
{
	size = sf::Vector2f(rect.width, rect.height);
	b2BodyDef bodyDef;
	
	b2Vec2 bodyPosition;
	
	if (!originBasedPosition)
	{
		bodyPosition.x = (rect.left + size.x / 2) / 32.f; //pixels to meters
		bodyPosition.y = (rect.top + size.y / 2) / 32.f; //pixels to meters
	}
	else
	{
		bodyPosition.x = rect.left / 32.f; //pixels to meters
		bodyPosition.y = rect.top / 32.f; //pixels to meters
	}

	bodyDef.position = bodyPosition;
	bodyDef.type = type;
	body = world->CreateBody(&bodyDef);

	b2PolygonShape bodyShape;
	bodyShape.SetAsBox(rect.width / 2 / 32.f, rect.height / 2 / 32.f); //pixels to meters

	b2FixtureDef fixtureDef;
	fixtureDef.friction = friction;
	fixtureDef.restitution = restitution;
	fixtureDef.isSensor = isSensor;
	fixtureDef.userData = userData;
	fixtureDef.shape = &bodyShape;
	body->CreateFixture(&fixtureDef);
}

void PhysicsObject::InitAsPolygonEdgeHybridBody(b2World *world, sf::FloatRect rect,
	b2BodyType type, void *userData, bool originBasedPosition, float friction,
	float restitution, bool isSensor)
{
	size = sf::Vector2f(rect.width, rect.height);
	b2BodyDef bodyDef;

	b2Vec2 bodyPosition;
	
	if (!originBasedPosition)
	{
		bodyPosition.x = (rect.left + size.x / 2) / 32.f; //pixels to meters
		bodyPosition.y = (rect.top + size.y / 2) / 32.f; //pixels to meters
	}
	else
	{
		bodyPosition.x = rect.left / 32.f; //pixels to meters
		bodyPosition.y = rect.top / 32.f; //pixels to meters
	}

	bodyDef.position = bodyPosition;
	bodyDef.type = type;
	body = world->CreateBody(&bodyDef);

	b2PolygonShape bodyShape;
	b2Vec2 vertices[4];
	vertices[0].Set(-size.x / 2 / 32.f, -(size.y-2) / 2 / 32.f);
	vertices[1].Set(size.x / 2 / 32.f, -(size.y-2) / 2 / 32.f);
	vertices[2].Set(size.x / 2 / 32.f, size.y / 2 / 32.f);
	vertices[3].Set(-size.x / 2 / 32.f, size.y / 2 / 32.f);
	bodyShape.Set(vertices, 4);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = friction;
	fixtureDef.restitution = restitution;
	fixtureDef.isSensor = isSensor;
	fixtureDef.userData = userData;
	fixtureDef.shape = &bodyShape;
	body->CreateFixture(&fixtureDef);

	b2FixtureDef fixtureDef2;
	b2EdgeShape bodyShape2;
	b2Vec2 v1 = b2Vec2(-size.x/2/32.f, -size.y/2/32.f),
		v2 = b2Vec2(size.x/2/32.f, -size.y/2/32.f),
		v0 = b2Vec2(-size.x/32.f, -size.y/2/32.f),
		v3 = b2Vec2(size.x/32.f, -size.y/2/32.f);
	fixtureDef2.shape = &bodyShape2;
	bodyShape2.Set(v1, v2);
	bodyShape2.m_vertex0 = v0;
	bodyShape2.m_vertex3 = v3;
	bodyShape2.m_hasVertex0 = true;
	bodyShape2.m_hasVertex3 = true;
	body->CreateFixture(&fixtureDef2);
}
