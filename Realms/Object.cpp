#include "Object.h"

Object::Object()
{
	id = "";
	size = sf::Vector2f(0, 0);
}

sf::FloatRect Object::GetScreenRect(sf::View &view)
{
	sf::FloatRect result;
	/*sf::Vector2f pos = GetPosition();
	sf::Vector2i sp = TranslateToScreenPos(view, pos);
	result.left = sp.x - view.getCenter().x;
	result.top = sp.y - view.getCenter().y;
	result.width = size.x;
	result.height = size.y;*/
	sf::Vector2f normalized = view.getTransform().transformPoint(GetPosition());
	sf::Vector2i pixel;
	sf::FloatRect viewport = view.getViewport();
	pixel.x = static_cast<int>((normalized.x + 1.f) / 2.f * viewport.width + viewport.left);
	pixel.y = static_cast<int>((-normalized.y + 1.f) / 2.f * viewport.height + viewport.top);
	result.left = pixel.x;
	result.top = pixel.y;
	result.width = size.x;
	result.height = size.y;
	return result;
}