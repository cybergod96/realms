#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include "AssetManager.h"
#include "Logger.h"
class Tileset
{
private:
	sf::Texture texture;
	std::vector<sf::IntRect> tiles;
	sf::Vector2i tileset_size; //tiles number horizontal x vertical
	std::string name;
public:
	Tileset();
	~Tileset();
	void Load(std::string name);
	sf::Vector2i GetSize() { return tileset_size; }
	sf::IntRect *GetTileRect(int tile_id);
	sf::IntRect *GetTileRect(int x, int y);
	std::string GetName() { return name; }
	sf::Texture &GetTexture() { return texture; }
	size_t GetTilesCount() { return tiles.size(); }
};

