#pragma once
#include <string>
#include <SFML/Graphics.hpp>
#include "utils.h"
#include "Logger.h"

class Object
{
protected:
	sf::Vector2f size;
	std::string id;
public:
	Object();
	virtual void onInit() = 0;
	virtual void onDeinit() = 0;
	virtual void onUpdate() = 0;
	virtual void onEvent(const sf::Event &e) = 0;
	virtual void onDraw(sf::RenderWindow &target) = 0;
	virtual void onDestroy() = 0;

	inline virtual std::string GetType() = 0;
	inline virtual std::string GetBase() { return "Object"; }
	virtual sf::FloatRect GetRect() = 0;
	virtual sf::Vector2f GetPosition() = 0;
	sf::FloatRect GetScreenRect(sf::View &view);
	sf::Vector2f GetSize() { return size; }
	void SetId(std::string id) { this->id = id; }
	std::string GetId() { return id; }
};