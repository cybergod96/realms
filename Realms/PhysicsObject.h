#pragma once
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <string>
#include "Logger.h"
#include "Object.h"
class PhysicsObject : public Object
{
protected:
	b2Body *body;
public:
	PhysicsObject();
	sf::Vector2f GetPositionPixels();
	sf::Vector2f GetPositionMeters();
	void SetPositionPixels(int x, int y);
	void SetPositionPixels(sf::Vector2i pos);
	float GetAngleDegrees() { return body->GetAngle() * 180 / b2_pi; }
	inline virtual std::string GetType() = 0;
	inline virtual std::string GetBase() { return "PhysicsObject"; }

	b2Body *GetBody() { return body; }
	b2Vec2 GetVelocityVector() { return body->GetLinearVelocity(); }
	virtual void onCollisionBegin(PhysicsObject *other) = 0;
	virtual void onCollisionEnd(PhysicsObject *other) = 0;

	void InitAsRectangularBody(b2World *world, sf::FloatRect rect, 
		b2BodyType type, void *userData = nullptr, bool originBasedPosition = false, 
		float friction = 0.2f, float restitution = 0.0f, bool isSensor = false);
	void InitAsEdgeBody(b2World *world, sf::FloatRect rect,
		b2BodyType type, void *userData = nullptr, bool originBasedPosition = false,
		float friction = 0.2f, float restitution = 0.0f, bool isSensor = false);
	void InitAsPolygonEdgeHybridBody(b2World *world, sf::FloatRect rect,
		b2BodyType type, void *userData = nullptr, bool originBasedPosition = false,
		float friction = 0.2f, float restitution = 0.0f, bool isSensor = false);
	~PhysicsObject();
};

