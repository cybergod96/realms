#include "MyContactListener.h"



MyContactListener::MyContactListener()
{
}


MyContactListener::~MyContactListener()
{
}

void MyContactListener::BeginContact(b2Contact * contact)
{
	if (!contact->GetFixtureA()->GetBody()->GetUserData() ||
		!contact->GetFixtureB()->GetBody()->GetUserData())
		return;
	PhysicsObject *a = static_cast<PhysicsObject *>(contact->GetFixtureA()->GetBody()->GetUserData());
	PhysicsObject *b = static_cast<PhysicsObject *>(contact->GetFixtureB()->GetBody()->GetUserData());

	a->onCollisionBegin(b);
	b->onCollisionBegin(a);
}

void MyContactListener::EndContact(b2Contact * contact)
{
	if (!contact->GetFixtureA()->GetBody()->GetUserData() ||
		!contact->GetFixtureB()->GetBody()->GetUserData())
		return;
	PhysicsObject *a = static_cast<PhysicsObject *>(contact->GetFixtureA()->GetBody()->GetUserData());
	PhysicsObject *b = static_cast<PhysicsObject *>(contact->GetFixtureB()->GetBody()->GetUserData());

	a->onCollisionEnd(b);
	b->onCollisionEnd(a);
}
