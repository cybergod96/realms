#pragma once
#include "Object.h"
#include "TileRepresentation.h"
class Tile : public Object, public TileRepresentation
{
public:
	Tile(Tileset *tileset, int tile_id, sf::Vector2f position);
	~Tile();

	void onInit();
	void onDeinit();
	void onUpdate();
	void onEvent(const sf::Event &e);
	void onDraw(sf::RenderWindow &target);
	void onDestroy();

	std::string GetType() { return "Tile"; }
	sf::FloatRect GetRect() { return sf::FloatRect(GetSpritePosition(), sf::Vector2f(32, 32)); }
	sf::Vector2f GetPosition() { return GetSpritePosition(); }
};

